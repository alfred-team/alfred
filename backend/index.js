const express = require('express');
var cors = require('cors');
const bodyParser = require('body-parser');

const API_PORT = 3001;
const app = express();
app.use(cors());
const router = express.Router();
const routerZones = express.Router();
const routerUsers = express.Router();

var firebase = require("firebase/app");

// Add the Firebase products that you want to use
require("firebase/auth");
require("firebase/database");

var firebaseConfig = {
  apiKey: "AIzaSyAG8i6jgWOQmE6qi18MZV5TqhCFZvDvRUE",
  authDomain: "alfred-be8f9.firebaseapp.com",
  databaseURL: "https://alfred-be8f9.firebaseio.com",
  projectId: "alfred-be8f9",
  storageBucket: "",
  messagingSenderId: "797649816381",
  appId: "1:797649816381:web:1704774fd8b98861421354"
};

firebase.initializeApp( firebaseConfig );

const database=require('./database');

//IMPORTANTE: Este print es para mostrar el estado de la peticion getGarages
//Problema: Por ahora solo pasa la info a la terminal.
//TODO: Fijarse como pasar a la pag la info
console.log(database.getGarages());


var address = new Object();
address.description = "Junin 55";
address.lat = 53.44;
address.lng = 44.44;
address.zone = "CABA";
var availableDays = [false, false, false, false, false, false, false];
//database.newGarage( "Garage de Jorgito", 'Jorgito tiene los mejores autos','9000','16','17', availableDays, address );



app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

// this is our get method
// this method fetches all available data in our database



// this is our update method
// this method overwrites existing data in our database
router.post('/update', (req, res) => {
  console.log('updating');

});

// this is our delete method
// this method removes existing data in our database
router.delete('/delete', (req, res) => {
  console.log('deleting');
});

router.post('/deleteGarage',(req,res) => {
  database.deleteGarage(req.body.gid,req.body.uid, res);
});

router.post('/editGarage',(req,res) => {
  database.editGarage(req.body.gid,req.body.newSettings,req.body.uid, res);
});

//THIS ONE WORKS
router.post('/addGarage',(req,res) => {
  console.log(req.body);
  database.newGarage(req.body.name,req.body.description,req.body.pricePerHour,req.body.openTime,req.body.closingTime,req.body.availableDays,req.body.address,req.body.uid, res, req.body.features, req.body.telephone);
});

// this is our create methid
// this method adds new data in our database
router.post('/putGarage', (req, res) => {
  //En req.body esta lo que envio por post
  var address = new Object();
  address.description = req.body.addressDescription;
  address.lat = req.body.lat;
  address.lng = req.body.lng;
  address.zone = req.body.zone;
  var availableDays = req.body.availableDays;
  //DOESNT WORK!!
  database.newGarage( req.body.name, req.body.description,req.body.pricePerHour,req.body.openTime,req.body.closingTime, availableDays, address, req.body.features, req.body.telephone );
  return res.json({ success: true });
});

router.get('/getGarages', (req, res) => {
  firebase.database().ref('garage').once('value').
  then(( snapshot )=>{
      return res.json({ success: true, data: snapshot.val() });
  }).catch((error) => {
    return res.json({ success: false, data: error });
  });
});

router.post('/bookGarage',(req,res) => {
  database.bookGarage(req.body.gid,req.body.uid,req.body.date,req.body.initHour,req.body.finalHour, res);
})

app.use('/garage', router);

routerZones.get('/getZones',(req,res) => {
  firebase.database().ref('zones').once('value')
    .then((snapshot) => {
      return res.json({ success:true, data: snapshot.val() });
    }).catch((error) => {
      return res.json({ success: false, data: error });
    })
})

app.use('/zones',routerZones);

routerUsers.post('/login',(req,res) => {
  console.log("POST DEL ROUTER");
  var email = req.body.email;
  var password = req.body.password;
  firebase
  .auth()
  .signInWithEmailAndPassword(email, password)
  .then(res2 => {
    console.log(res2.user.uid);
    firebase.database().ref('users/' + res2.user.uid).once('value')
      .then((snapshot) => {
        res.json({ success: true, uid: res2.user.uid, name:snapshot.val().name, surname: snapshot.val().surname });
      }).catch((error) => {
        res.json({ success: false, error: error });
      })
  })
  .catch(e => {
    console.log(e);
    res.json({ success: false, error: e });
  });
});

routerUsers.post('/signup',(req,res) => {
  console.log("POST DEL router de sign up");
  var email = req.body.email;
  var password = req.body.password;
  var name = req.body.name;
  var surname= req.body.surname;

  firebase
    .auth()
    .createUserWithEmailAndPassword(email, password)
  .then(user => {
    console.log(user.user.uid);
    var uid = user.user.uid;
    //No estaria de mas poner el template de los usuarios aca...
    firebase.database().ref('users').update({
      [uid] : {
        name,
        surname,
        email
      }
    });
    res.json({ success:true, uid });
  })
  .catch(e => {
    console.log(e);
    res.json({ success: false });
  });
});

app.use('/user', routerUsers);

//database.bookGarage('AVXAPzpK9s','1KdqhI100rgBFsVNfwyLYaJxIK03',new Date(),14,17, null);

// launch our backend into a port
app.listen(API_PORT, () => console.log(`LISTENING ON PORT ${API_PORT}`));

/*
// New functions testing
var newProfileSettings = {
  nombre : 'Igna',
  apellido : 'Test',
  email: 'email@email.com',
  password: 'password123'
}

var newGarageSettings = {
  owner: 'email@email.com'
}

var car = {
  size : '20x20x20',
  brand : 'Mercedes',
  model : 'Benz'
}

database.newVehicle( 'email@email.com', 'password123', car );
database.editProfile('email@email.com', 'password123', newProfileSettings );
database.editGarage( 1, newGarageSettings );
database.getOwnerGarages('oObzguVW1xeDmdQXyhwLN9HvFYy2').then(snapshot => {
  console.log( snapshot );
});
database.deleteGarage(13);*/

