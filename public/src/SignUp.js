import React from 'react';
import './css/SignUp.css';
import './css/LogIn.css';

import Card from "@material-ui/core/Card";
import Button from "@material-ui/core/Button";
import CardActions from "@material-ui/core/CardActions";
import TextField from "@material-ui/core/TextField";


class SignUp extends React.Component {
    render() {
        var email;
        var password;
        var name;
        var surname;
        return (
            <Card className={"signCard"}>
                <h2>Alfred</h2>
                <br/>
                <h1> Sign Up </h1>
                <p> You may use your Google Account for immediate sign up </p>

                <form onSubmit={e => {
                    e.preventDefault();
                    return false;
                }}>
                    <TextField
                        id="name"
                        label={"name"}
                        type={"text"}
                        placeholder={"Alfred"}
                        fullWidth
                        onChange={e => {
                            name = e.target.value;
                        }}
                        margin="normal"
                        variant="outlined"
                    />
                    <TextField
                        id="surname"
                        label={"surname"}
                        type={"text"}
                        placeholder={"Alfred"}
                        fullWidth
                        onChange={e => {
                            surname = e.target.value;
                        }}
                        margin="normal"
                        variant="outlined"
                    />
                    <TextField
                        id="email"
                        label={"email"}
                        type={"email"}
                        placeholder={"Alfred@my-mail.com"}
                        fullWidth
                        onChange={e => {
                            email = e.target.value;
                        }}
                        margin="normal"
                        variant="outlined"
                    />
                    <TextField
                        id="password"
                        label={"password"}
                        type={"password"}
                        placeholder={"********"}
                        fullWidth
                        onChange={e => {
                            password = e.target.value;
                        }}
                        margin="normal"
                        variant="outlined"
                    />
                    <hr/>

                    <CardActions className={"signCard-buttons"}>
                        <Button variant={"outlined"} size="large" type="button"
                                onClick={() => this.props.handleSignUp(email, password, name, surname)}>
                            Sign Up
                        </Button>

                    </CardActions>
                </form>
            </Card>
        );
    }
}

export default SignUp;