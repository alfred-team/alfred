//import React from 'react';
import axios from 'axios';

const database = {
    getZones: async function (other) {
        await fetch('http://localhost:3001/zones/getZones')
            .then((data) => data.json())
            .then((res) => {
                //console.log({ data: res.data });

                var array = [];
                for (var i in res.data) {
                    if (res.data[i] !== null) {
                        var obj = {};
                        obj.code = res.data[i].code;
                        obj.lat = res.data[i].lat;
                        obj.lng = res.data[i].lng;
                        obj.name = res.data[i].name;
                        array.push(obj);
                    }
                }
                other.setState({zones: array});

                return res.data;
                //This is the way to iterate for each car zone!
                //for(var i in res.data){
                //  console.log(res.data[i]);
                //}
            });
    },
    getGarages: async function (other) {
        //Debera hacer el checkeo
        try {
            await fetch('http://localhost:3001/garage/getGarages')
                .then((data) => data.json())
                .then((res) => {
                    //console.log({ data: res.data });
                    //console.log(res.data);
                    var array = [];
                    for (var i in res.data) {
                        if (res.data[i] !== null) {
                            var obj = {};
                            //console.log(res.data[i]);
                            obj.description = res.data[i].description;
                            obj.address = res.data[i].address;
                            obj.availableDays = res.data[i].availableDays;
                            obj.openTime = res.data[i].openTime;
                            obj.closingTime = res.data[i].closingTime;
                            obj.img = res.data[i].img;
                            obj.pricePerHour = res.data[i].pricePerHour;
                            obj.name = res.data[i].name;
                            obj.owner = res.data[i].owner;
                            obj.gid = i;
                            obj.features = res.data[i].features;
                            obj.telephone = res.data[i].telephone;
                            array.push(obj);
                        }
                    }
                    console.log(array);
                    other.setState({garages: array});
                    return res.data;
                });
        } catch (err) {
            console.log(err);
        }
    },
    putGarage: function (addressDescription, lat, lng, zone, availableDays, name, description, pricePerHour, openTime, closingTime, features, telephone) { //Aca deberia poner los parametros que llegan
        axios.post('http://localhost:3001/garage/putGarage', {
            addressDescription,
            lat,
            lng,
            zone,
            availableDays,
            name,
            description,
            pricePerHour,
            openTime,
            closingTime,
            features,
            telephone
        });
    },
    /*const user = await database.login("gas2@gmail.com", "123456789");
    console.log(user);
    if(user.data.success ===true){
      //Store the uid thats placed in user.data.uid
      //this.setState({ uid: user.data.uid })
      console.log(user.data.uid);
    }else{
      //Throw visual error
    }*/
    login: async function (email, password) {
        const user = await axios.post('http://localhost:3001/user/login', {
            email,
            password
        });
        return user;
    },
    /*const user = await database.signUp("gas2@gmail.com", "123456789", "Gaston", "Lifschitz");
    console.log(user);
    if(user.data.success ===true){
      //Store the uid thats placed in user.data.uid
      //this.setState({ uid: user.data.uid })
      console.log(user.data.uid);
    }else{
      //Throw visual error
    }*/
    signUp: async function (email, password, name, surname) {
        const user = await axios.post('http://localhost:3001/user/signup', {
            email,
            password,
            name,
            surname
        });
        return user;
    },
    /*const res = await database.deleteGarage(8,"igna@sampedro.com");
    console.log('delete:', res.data.success);
    */
    deleteGarage: async function (gid, uid) {
        const status = await axios.post('http://localhost:3001/garage/deleteGarage', {
            gid,
            uid
        });
        return status;
    },
    /*
    const res = await database.editGarage(gid,newSettings,uid)
    console.log(res.data.success) -> true=lo borro, false= no lo borro (err)
    */
    editGarage: async function (gid, newSettings, uid) {
        const status = await axios.post('http://localhost:3001/garage/editGarage', {
            gid,
            newSettings,
            uid
        });
        return status;
    },
    addGarage: async function (name, description, pricePerHour, openTime, closingTime, availableDays, address, uid, features, telephone) {
        const status = await axios.post('http://localhost:3001/garage/addGarage', {
            name,
            description,
            pricePerHour,
            openTime,
            closingTime,
            availableDays,
            address,
            uid,
            features,
            telephone
        });
        return status;
    },
    bookGarage: async function (gid, uid, date, initHour, finalHour) {
        if (!(date instanceof Date)) {
            return {
                success: false,
                error: "Not a valid date"
            };
        }
        if (initHour < 0 || finalHour >= 24 || initHour >= 24 || finalHour < 0) {
            return {
                success: false,
                error: "Not a valid date"
            };
        }
        const status = await axios.post('http://localhost:3001/garage/bookGarage', {
            gid,
            uid,
            date,
            initHour,
            finalHour
        });
        return status;
    }

};

export default database;


/**
 * Para implementar en front hay que:
 * import database from './middleware/database.js'; En archivo
 * LLamar a la funcion como database.asdfasdfa
 * Ejemplo con put database.putGarage("Av. del Libertador 3000",-30.5,-55.3,'BEL',[false,false,false,false,false,false,false],"Estacionamiento ento","El mejor Estacionamiento", 90, 12,19);
 **/
