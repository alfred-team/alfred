import React from 'react';
import './css/book.css';

import Card from '@material-ui/core/Card';
import TextField from '@material-ui/core/TextField';
import CardActions from '@material-ui/core/CardActions';
import Button from "@material-ui/core/Button";


class Book extends React.Component {
    render() {
        var date;
        var init_time;
        var end_time;

        return (
            <Card className={"bookCard"}>
                <h2>Alfred</h2>
                <br/>
                <h1> Book your place! </h1>

                <form className={"bookCard-content"} onSubmit={e => {
                    e.preventDefault();
                    return false;
                }}>
                    <TextField
                        id="Date"
                        label={"Date"}
                        type={"Date"}
                        placeholder={"Format: yyyy/mm/dd"}
                        fullWidth
                        onChange={e => {
                            date = e.target.value;
                        }}
                        margin="normal"
                        variant="outlined"
                    />
                    <TextField
                        id="Start Time"
                        label={"From"}
                        placeholder={"3 hours"}
                        fullWidth
                        onChange={e => {
                            init_time = e.target.value;
                        }}
                        margin="normal"
                        variant="outlined"
                    />
                    <TextField
                        id="End Time"
                        label={"To"}
                        placeholder={"3 hours"}
                        fullWidth
                        onChange={e => {
                            end_time = e.target.value;
                        }}
                        margin="normal"
                        variant="outlined"
                    />
                    <hr/>

                    <CardActions className={"bookCard-buttons"}>
                        <Button variant={"outlined"} size="large" type="button"
                                onClick={() => this.props.handleBooking(date, init_time, end_time)}>Book now</Button>
                    </CardActions>
                </form>
            </Card>
        );
    }
}

export default Book;
