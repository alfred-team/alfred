import React from 'react';
import './css/LogIn.css';

import GoogleLogin from 'react-google-login';
import Card from '@material-ui/core/Card';
import TextField from '@material-ui/core/TextField';
import CardActions from '@material-ui/core/CardActions';
import Button from "@material-ui/core/Button";


class LogIn extends React.Component {
    render() {
        var email;
        var password;
        return (
            <Card className={"signCard"}>
                <h2>Alfred</h2>
                <br/>
                <h1> Log In </h1>
                <p> You may login with Google or with an Alfred account </p>

                <form className={"signCard-content"} onSubmit={e => {
                    e.preventDefault();
                    return false;
                }}>
                    <TextField
                        id="email"
                        label={"email"}
                        type={"email"}
                        placeholder={"Alfred@my-mail.com"}
                        fullWidth
                        onChange={e => {
                            email = e.target.value;
                        }}
                        margin="normal"
                        variant="outlined"
                    />
                    <TextField
                        id="password"
                        label={"password"}
                        type={"password"}
                        placeholder={"********"}
                        fullWidth
                        onChange={e => {
                            password = e.target.value;
                        }}
                        margin="normal"
                        variant="outlined"
                    />
                    <hr/>

                    <CardActions className={"signCard-buttons"}>
                        <GoogleLogin
                            clientId="797649816381-sl05foo5gtk3o15krtlgvf7s43t5ra9a.apps.googleusercontent.com"
                            buttonText="Login"
                            onSuccess={(response) => console.log(response)}
                            onFailure={(response) => console.log(response)}
                            cookiePolicy={'single_host_origin'}
                        />
                        <Button variant={"outlined"} size="large" type="button"
                                onClick={() => this.props.handleLogIn(email, password)}>Login</Button>

                    </CardActions>
                </form>
            </Card>
        );
    }
}

export default LogIn;
