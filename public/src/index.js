import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import * as serviceWorker from './serviceWorker';
import Geocode from "react-geocode";

Geocode.setApiKey("AIzaSyCdWlyohFHfZJXrcm2rmPh5lEGU44aE8ug");
Geocode.setLanguage("es");
Geocode.setRegion("ar");


ReactDOM.render(<App/>, document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
