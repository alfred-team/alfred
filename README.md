# Project Alfred

Project Alfred is a Single-Page Website that allows car drivers to find relevant lots to park. Its features are focused on CABA but it is scalable for other locations

## Prerequisites

Alfred requires Node.js (tested on 6.12 forward) in order to install the application.
It also requires Internet Connection in order to be able to communicate with the Database to send and obtain data.

## User Guide (Installation)

### Static (with build & serve)

1- Clone the Repository

2- Install node dependencies for both backend and frontend.

```bash
cd ./public && npm install && cd ..
cd ./backend && npm install && cd ..
npm install
```
3- Build the project with the following command:
```bash
npm run build
```
4- Run the server in a new bash terminal with:
```bash
npm run backend
```
5- Serve the build created on step 3 with:
```bash
npm run serve
```
6- Go to link indicated by the terminal after executing the serve. (The link should be copied on the clipboard)

7- On finish, terminate both the build and the backend with Ctrl+C

### Dynamic (with run start)
1- Do steps 1 & 2 from "Static" guide
2- Start the Application with:
```bash
npm run start
```
3- On finish, terminate the process with Ctrl+C

## Navigation

The Webapp is centered about 2 main features: "Search Lots" and "Manage Lots", the latter one is only available for logged in users.

### Search Lots

Alfred has a search bar where users may also filter their desired lots with a Zone filter for each zone in CABA.
The map on the left shows with markers the available lots that match with the selected results.

Upon clicking on a Lot card, the map will center on the selected lot, and upon clicking on a marker a card with the lot's information will appear as a Info Window.

### Manage Lots

Available only for logged in users. Only the lots belonging to the user are shown for both the list and the Map.
Users can add new lots from this section.

The lot cards for this section has 2 buttons: Edit Lot and Delete Lot

#### New lots & Edit Lot

Users must fill a form that requests information about the lot they want to register

In case of editing an already existing lot, the current data is loaded.

#### Delete Lot

A dialog appears asking a confirmation for the action in order to avoid accidental deletions.

## Testing
Tests using Mocha.js with Chai have been created in order to verify that the database returns the requested data in the desired format.

The unit tests for Alfred can be found within the backend directory.

To run them simple start the server using:
```bash
npm run backend
```
and then run 
```bash
cd backend && npm run test %% cd ..
```

## Authors
* Gaston Lifschitz
* Cristobal Rojas
* Enrique Tawara
* Ignacio Sampedro
* Manuel Luque Meijide