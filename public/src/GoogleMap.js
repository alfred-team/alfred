import React from 'react';
import {GoogleApiWrapper, InfoWindow, Map, Marker} from 'google-maps-react';
import './css/GoogleMap.css';
import Card from "@material-ui/core/Card";
import LotBasicData from "./LotBasicData";

/**
 * @return {null}
 */
function DisplayLot(props) {
    if (!props.show) {
        return null;
    }

    return (
        <LotBasicData garage={props.garage}/>
    );
}

class GoogleMap extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            showingInfoWindow: false,
            activeMarker: {},
            selectedLot: {},
        };
    }


    onMarkerClick = (props, marker) => {
        this.setState({
            selectedLot: props.garage,
            activeMarker: marker,
            showingInfoWindow: true
        });
    };

    onMapClicked = () => {
        if (this.state.showingInfoWindow) {
            this.setState({
                showingInfoWindow: false,
                activeMarker: null
            })
        }
    };

    displayMarkers = () => {
        return this.props.garages.map((garage, index) => {
            //Ask how to properly do this condition
            return <Marker
                key={index}
                id={index}
                position={{lat: garage.address.lat, lng: garage.address.lng,}}
                onClick={this.onMarkerClick}
                garage={garage}
            />
        })
    };

    render() {
        var centerLat;
        var centerLng;
        if (this.props.activeLot == null || typeof this.props.activeLot === "undefined") {
            centerLat = this.props.activeZone.lat;
            centerLng = this.props.activeZone.lng;
        } else {
            centerLat = this.props.activeLot.address.lat;
            centerLng = this.props.activeLot.address.lng;
        }
        return (
            <div>
                <Map
                    google={this.props.google}
                    zoom={15}
                    className="google-map"
                    initialCenter={{lat: this.props.activeZone.lat, lng: this.props.activeZone.lng}}
                    center={{lat: centerLat, lng: centerLng}}
                    onClick={this.onMapClicked}
                >
                    {this.displayMarkers()}
                    <InfoWindow
                        marker={this.state.activeMarker}
                        visible={this.state.showingInfoWindow}>
                        <Card className={"info-card"}>
                            <DisplayLot show={this.state.showingInfoWindow} garage={this.state.selectedLot}/>
                        </Card>
                    </InfoWindow>
                </Map>

            </div>
        );
    }
}

export default GoogleApiWrapper({
    apiKey: 'AIzaSyCdWlyohFHfZJXrcm2rmPh5lEGU44aE8ug'
})(GoogleMap);