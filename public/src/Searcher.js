import React from 'react';
import SearchBar from './SearchBar';
import LotList from './LotList';
import './css/Searcher.css';


class Searcher extends React.Component {
    render() {
        return (
            <div>
                <SearchBar
                    activeZone={this.props.activeZone}
                    zones={this.props.zones}
                    changeActiveZone={(zone) => this.props.changeActiveZone(zone)}
                    onChange={(string) => this.props.onChange(string)}
                />
                <h3>Results</h3>
                <LotList
                    garages={this.props.garages}
                    activeLot={this.props.activeLot}
                    handleBook={(garage) => this.props.handleBook(garage)}
                    changeActiveLot={(lot) => this.props.changeActiveLot(lot)}
                    changeActiveMode={(mode) => this.props.changeActiveMode(mode)}
                />
            </div>
        );
    }
}

export default Searcher;
