import React from 'react';
import Searcher from './Searcher';
import GoogleMap from './GoogleMap';
import AddLot from './AddLot';
import LogIn from './LogIn';
import SignUp from './SignUp';
import Book from './book';

import './css/Stage.css';
import EditLotList from "./EditLotList";


class Stage extends React.Component {

    searchGarage(string) {
        console.log(string);
        //TODO: Implement function
    }

    renderStage() {
        var userGarageCode;
        var userGarageLat;
        var userGarageLng;
        if (this.props.userGarages[0] == null || typeof this.props.userGarages[0] === "undefined") {
            userGarageCode = "BEL";
            userGarageLat = -34.558855;
            userGarageLng = -58.450451;
        } else {
            userGarageCode = this.props.userGarages[0].address.zone;
            userGarageLat = this.props.userGarages[0].address.lat;
            userGarageLng = this.props.userGarages[0].address.lng;
        }
        switch (this.props.activeMode) {
            case "search":
                return (
                    <div className="grid-container stage-item">
                        <div className="grid-item">
                            <Searcher
                                activeZone={this.props.activeZone}
                                zones={this.props.zones}
                                changeActiveZone={(zone) => this.props.changeActiveZone(zone)}
                                onChange={(string) => this.searchGarage(string)}
                                garages={this.props.garages}
                                changeActiveLot={(lot) => this.props.changeActiveLot(lot)}
                                handleBook={(garage) => this.props.handleBook(garage)}
                                changeActiveMode={(mode) => this.props.changeActiveMode(mode)}
                            />
                        </div>
                        <div className="grid-item">
                            <GoogleMap
                                activeZone={this.props.activeZone}
                                garages={this.props.garages}
                                activeLot={this.props.activeLot}
                            />
                        </div>
                    </div>
                );
            case "add":
                return (
                    <div className="grid-container stage-item">
                        <div className="grid-item">
                            <div>
                                <AddLot
                                    handleAddLot={(lot) => this.props.handleAddLot(lot)}
                                    zones={this.props.zones}
                                />
                                <h3>Existing lots</h3>
                                <EditLotList
                                    handleDeleteLot={(garage) => this.props.handleDeleteLot(garage)}
                                    handleAddLot={(lot) => this.props.handleAddLot(lot)}
                                    zones={this.props.zones}
                                    changeActiveLot={(lot) => this.props.changeActiveLot(lot)}
                                    garages={this.props.userGarages}
                                    handleEditLot={(oldGarage, newGarage) => this.props.handleEditLot(oldGarage, newGarage)}
                                />
                            </div>
                        </div>
                        <div className="grid-item">
                            <GoogleMap
                                activeZone={{
                                    code: userGarageCode,
                                    lat: userGarageLat,
                                    lng: userGarageLng,
                                    name: "Dummy Zone",
                                }}
                                garages={this.props.userGarages}
                                activeLot={this.props.activeLot}
                            />
                        </div>
                    </div>
                );
            case "login":
                return (
                    <div className={"stage-item card-item"}>
                        <LogIn handleLogIn={(email, password) => this.props.handleLogIn(email, password)}/>
                    </div>
                );
            case "join":
                return (
                    <div className={"stage-item card-item"}>
                        <SignUp
                            handleSignUp={(email, password, name, surname) => this.props.handleSignUp(email, password, name, surname)}/>
                    </div>
                );
            case "book":
                return (
                    <div className={"stage-item card-item"}>
                        <Book
                            changeActiveMode={(mode) => this.props.changeActiveMode(mode)}
                            handleBooking={(date, init_time, end_time) => this.props.handleBooking(date, init_time, end_time)}
                        />
                    </div>
                );
            default:
                return (
                    <div>
                        <h1>Oops! There was an error in our end</h1>
                        <p>Try starting again from our home page</p>
                    </div>
                )
        }
    }

    render() {
        return (
            <div>
                {this.renderStage()}
            </div>
        );
    }
}

export default Stage;

//Legacy code for reference
/*
			<div className={"row"}>
				<div className={"column left"}>
				<Searcher
					activeZone={this.props.activeZone}
					zones={this.props.zones}
					changeActiveZone={(zone) => this.props.changeActiveZone(zone)}
					onChange={(string) => this.searchGarage(string)}
					garages={this.props.garages}
				/>
				</div>
				<div className={"column right"}>
					<GoogleMap
						activeZone={this.props.activeZone}
						garages={this.props.garages}
					/>
				</div>
			</div>


			 */
