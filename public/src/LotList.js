import React from 'react';
import LotItem from './LotItem';
import './css/LotList.css';


class LotList extends React.Component {
    render() {
        var changeActiveLot = this.props.changeActiveLot;
        var changeActiveMode = this.props.changeActiveMode;
        var handleBook = this.props.handleBook;
        var garages = this.props.garages.slice();
        var garageList = garages.map(function (garage, index) {
            return <li key={index}><LotItem handleBook={handleBook} changeActiveLot={changeActiveLot} garage={garage}
                                            changeActiveMode={changeActiveMode}/></li>
        });
        return (
            <div className={"lotList"}>
                <ul>
                    {garageList}
                </ul>
            </div>
        );
    }
}

export default LotList;
