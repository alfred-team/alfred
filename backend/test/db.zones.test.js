const express = require('express');
var cors = require('cors');
const app = express();
app.use(cors());
var request = require("request");


const assert = require('assert');
//const expect = require('expect');
const expect = require('chai').expect;
//const should = require('chai').should();


describe('Alfred Zones Database Test', function () {
    //getzones
    it('should return all zones', function (done) {
        var url = "http://localhost:3001/zones/getZones";
        request(url, function(error,response,body){
            var obj = JSON.parse(body);
            expect(error).to.be.null;
            expect(obj.success).to.be.true;
            if(obj.data != undefined){
                var count = 0;
                for(var i in obj.data){
                    count++;
                    expect(obj.data[i].code).to.be.a('string');
                    expect(obj.data[i].lat).to.be.a('string');
                    expect(obj.data[i].lng).to.be.a('string');
                    expect(obj.data[i].name).to.be.a('string');
                }
                expect(count === 48 ).to.be.true;
            }
            done();
        });
    })
});