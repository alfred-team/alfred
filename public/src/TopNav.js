import React from 'react';
//import UserInfo from './UserInfo';
import './css/TopNav.css';
//from material-ui
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import Button from "@material-ui/core/Button";
import IconButton from '@material-ui/core/IconButton';

import AccountCircleIcon from '@material-ui/icons/AccountCircle';
import MenuIcon from '@material-ui/icons/Menu';
import DirectionsCarIcon from '@material-ui/icons/DirectionsCar';
import HomeWorkIcon from '@material-ui/icons/HomeWork';

//import ReactDOM from 'react-dom';


class TopNav extends React.Component {
    renderLogin() {
        if (this.props.loggedIn) {
            return (
                <IconButton edge="start" className={"profileButton topIcon"} color="inherit" aria-label="profile">
                    <AccountCircleIcon/>
                    <p>{this.props.userInfo.email}</p>
                </IconButton>
            );
        } else {
            return (
                <div>
                    <IconButton onClick={() => this.props.changeActiveMode("login")} edge="start"
                                className={"profileButton topIcon"} color="inherit" aria-label="profile">
                        Log in
                    </IconButton>

                    <IconButton onClick={() => this.props.changeActiveMode("join")} edge="start"
                                className={"profileButton topIcon"} color="inherit" aria-label="profile">
                        Sign up
                    </IconButton>
                </div>
            );
        }
    }

    renderNav() {
        if (this.props.loggedIn) {
            return (
                <div className={"tab-navigator"}>
                    <Button onClick={() => this.props.changeActiveMode("search")}
                            className={(this.props.activeMode === "search") ? "page current" : "page"}
                            color={"inherit"}><DirectionsCarIcon className={"topIcon"} color="inherit"/><h2>Search
                        lot</h2></Button>
                    <Button onClick={() => this.props.changeActiveMode("add")}
                            className={(this.props.activeMode === "add") ? "page current" : "page"}
                            color={"inherit"}><HomeWorkIcon className={"topIcon"} color="inherit"/><h2>My lots</h2>
                    </Button>
                </div>
            );
        } else {
            return (
                <div className={"tab-navigator"}>
                    <Button onClick={() => this.props.changeActiveMode("search")}
                            className={(this.props.activeMode === "search") ? "page current" : "page"}
                            color={"inherit"}><DirectionsCarIcon className={"topIcon"} color="inherit"/><h2>Search
                        lot</h2></Button>
                </div>
            );
        }
    }

    render() {
        return (
            <AppBar>
                <Toolbar className={"nav-container"}>
                    <div className={"appLogo"}>
                        <Button onClick={() => this.props.changeActiveMode("search")} className={"brand"}
                                color={"inherit"}><h1>Alfred</h1></Button>
                    </div>

                    {this.renderNav()}

                    <div className={"grow"}/>

                    {this.renderLogin()}

                    <IconButton edge="start" className={"menuButton topIcon"} color="inherit" aria-label="menu">
                        <MenuIcon/>
                    </IconButton>
                </Toolbar>
            </AppBar>
        );
    }
}

export default TopNav;
