import React from 'react';
import {GoogleApiWrapper, Map, Marker} from 'google-maps-react';
import './css/MiniMap.css';


class MiniMap extends React.Component {
    render() {

        const style = {
            width: '700px',
            height: '70%',
            position: 'fixed'
        };

        return (
            <div>
                <Map
                    google={this.props.google}
                    zoom={15}
                    style={style}
                    initialCenter={{lat: this.props.lat, lng: this.props.lng,}}
                    center={{lat: this.props.lat, lng: this.props.lng}}
                >
                    <Marker
                        position={{lat: this.props.lat, lng: this.props.lng,}}
                    />
                </Map>
            </div>
        );
    }
}

export default GoogleApiWrapper({
    apiKey: 'AIzaSyCdWlyohFHfZJXrcm2rmPh5lEGU44aE8ug'
})(MiniMap);