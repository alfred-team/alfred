import React from 'react';
import EditLotItem from './EditLotItem';
import './css/LotList.css';


class EditLotList extends React.Component {
    render() {
        var changeActiveLot = this.props.changeActiveLot;
        let zones = this.props.zones;
        var garages = this.props.garages.slice();

        var garageList = garages.map((garage, index) => {
            return <li key={index}>
                <EditLotItem
                    handleDeleteLot={(garage) => this.props.handleDeleteLot(garage)}
                    handleAddLot={(lot) => this.props.handleAddLot(lot)}
                    handleEditLot={(oldGarage, newGarage) => this.props.handleEditLot(oldGarage, newGarage)}
                    zones={zones}
                    garage={garage}
                    changeActiveLot={changeActiveLot}
                />
            </li>
        });
        return (
            <div className={"lotList"}>
                <ul>
                    {garageList}
                </ul>
            </div>
        );
    }
}

export default EditLotList;