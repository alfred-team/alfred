import React from 'react';
import './css/LotBasicData.css';
import './css/AddLot.css'
//from material ui
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import Button from '@material-ui/core/Button';
import LotBasicData from "./LotBasicData";
import Modal from '@material-ui/core/Modal';
import FormControlLabel from "@material-ui/core/FormControlLabel";
import {Checkbox} from "@material-ui/core";
import TextField from "@material-ui/core/TextField";
import FormControl from "@material-ui/core/FormControl";
import Select from "@material-ui/core/Select";
import InputLabel from "@material-ui/core/InputLabel";
import Input from "@material-ui/core/Input";
import InputAdornment from "@material-ui/core/InputAdornment";
import FormGroup from "@material-ui/core/FormGroup";
import MiniMap from './MiniMap';
import Geocode from "react-geocode";


class EditLotItem extends React.Component {

    constructor(props) {
        super(props);
        var garageZone = this.props.zones.find((zone) => {
            return zone.code === this.props.garage.address.zone;
        });
        console.log(garageZone);
        this.state = {
            edit: false,
            delete: false,
            lat: this.props.garage.address.lat,
            lng: this.props.garage.address.lng,
            activeZone: {
                name: garageZone.name,
                lat: garageZone.lat,
                lng: garageZone.lng,
                code: garageZone.code,
            },
            lot: {...this.props.garage},
        };
    }

    editGarage = () => {
        console.log('edit garage' + this.props.garage.name);
    };

    handleEditOpen = () => {
        this.setState({
                edit: true,
                lot: {...this.props.garage}
            },
        );
    };

    handleEditClose = () => {
        this.setState({
            edit: false,
            lot: {...this.props.garage},
        });
    };

    handleDeleteOpen = () => {
        this.setState({delete: true});
    };

    handleDeleteClose = () => {
        this.setState({delete: false});
    };

    displayOptions = () => {
        return this.props.zones.map((zone, index) => {
            return <option key={index} value={zone}>{zone.name}</option>
        })
    };

    handleGeocode = (address) => {
        Geocode.fromAddress(address).then(
            response => {
                const {lat, lng} = response.results[0].geometry.location;
                this.setState({
                    lat: lat,
                    lng: lng,
                })
            },
            error => {
                console.error(error);
            }
        );
    };

    render() {
        let garage = this.props.garage;

        let days = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday"];
        let week = [];

        for (let i = 0; i < 7; i++) {
            week.push(
                <FormControlLabel key={i}
                                  control={
                                      <Checkbox color="primary" onChange={() => {
                                          let newLot = this.state.lot;
                                          let newDays = newLot.availableDays.slice();
                                          newDays[i] = !newDays[i];
                                          newLot.availableDays = newDays;
                                          this.setState({
                                              lot: newLot,
                                          });
                                      }}
                                                defaultChecked={this.state.lot.availableDays[i]}/>
                                  }
                                  label={days[i]}
                                  labelPlacement="top"
                />
            );
        }

        return (
            <div>
                <Card onClick={() => this.props.changeActiveLot(garage)} className={"LotCard"}>
                    <LotBasicData garage={garage}/>

                    <CardActions className={"options"}>
                        <Button color={"primary"} onClick={this.handleEditOpen}>Edit this lot</Button>
                        <Button color={"secondary"} onClick={this.handleDeleteOpen}>Delete lot</Button>
                    </CardActions>
                </Card>

                <Modal open={this.state.edit}
                       onClose={this.handleEditClose}
                       className={"dialog-container"}>
                    <div className="dialog-main">

                        <form
                            onSubmit={e => {
                                //console.log("Submitting");
                                e.preventDefault();
                                this.props.handleEditLot(garage, this.state.lot);
                                this.handleEditClose();
                                return false;
                            }}>
                            <div className={"hor-flex-container"}>

                                <div className={"lot-flex-container half-size"}>

                                    <h1>Edit existing lot</h1>
                                    <h4>Editing lot '{garage.name}'.</h4>

                                    <div className={"text-input"}>
                                        <p>Lot name</p>
                                        <TextField
                                            id="name"
                                            placeholder={"my lot's name is..."}
                                            fullWidth
                                            defaultValue={this.state.lot.name}
                                            onChange={e => {
                                                let newLot = this.state.lot;
                                                newLot.name = e.target.value;
                                                this.setState({
                                                    lot: newLot,
                                                });
                                            }}
                                            type={"text"}
                                        />
                                    </div>

                                    <div className={"text-input"}>
                                        <p>Lot address</p>
                                        <TextField
                                            id="address"
                                            placeholder={"my lot is in..."}
                                            fullWidth
                                            defaultValue={this.state.lot.address.description}
                                            onChange={e => {
                                                let newLot = this.state.lot;
                                                let newAddress = {...newLot.address};
                                                newAddress.description = e.target.value;
                                                newLot.address = newAddress;
                                                this.setState({
                                                    lot: newLot,
                                                });
                                                this.handleGeocode(e.target.value);
                                            }}
                                            type={"text"}
                                        />
                                    </div>

                                    <FormControl className={"text-input"}>
                                        <p>Zone</p>
                                        <Select
                                            value={this.state.activeZone}
                                            onChange={(event: object) => {
                                                let newLot = this.state.lot;
                                                let newAddress = {...newLot.address};
                                                newAddress.zone = event.target.value.code;
                                                newLot.address = newAddress;
                                                this.setState({
                                                    lot: newLot,
                                                    activeZone: event.target.value,
                                                });
                                            }}
                                            inputProps={{
                                                name: 'zone',
                                                id: 'zone-native-simple',
                                            }}
                                            renderValue={(value: any) => {
                                                return this.state.activeZone.name;
                                            }}
                                        >
                                            {this.displayOptions()}
                                        </Select>
                                    </FormControl>

                                    <div className={"hor-flex-container"}>
                                        <div className={"text-input quarter-size"}>
                                            <p>Opening time</p>
                                            <TextField
                                                id="opening-time"
                                                name="openingTime"
                                                type="time"
                                                defaultValue={this.state.lot.openTime}
                                                onChange={e => {
                                                    let newLot = this.state.lot;
                                                    newLot.openTime = e.target.value;
                                                    this.setState({
                                                        lot: newLot,
                                                    });
                                                }}
                                                inputProps={{
                                                    step: 300, // 5 min
                                                }}
                                            />
                                        </div>

                                        <div className={"text-input quarter-size"}>
                                            <p>Closing time</p>
                                            <TextField
                                                id="opening-time"
                                                name="closingTime"
                                                type="time"
                                                defaultValue={this.state.lot.closingTime}
                                                onChange={e => {
                                                    let newLot = this.state.lot;
                                                    newLot.closingTime = e.target.value;
                                                    this.setState({
                                                        lot: newLot,
                                                    });
                                                }}
                                                inputProps={{
                                                    step: 300, // 5 min
                                                }}
                                            />
                                        </div>
                                    </div>

                                    <div className={"text-input"}>
                                        <p>Phone number</p>
                                        <TextField
                                            id="phone"
                                            placeholder={"12 1234-5678"}
                                            fullWidth
                                            onChange={e => {
                                                let newLot = this.state.lot;
                                                newLot.telephone = e.target.value;
                                                this.setState({
                                                    lot: newLot,
                                                });
                                            }}
                                            type={"text"}
                                        />
                                    </div>

                                    <div className={"text-input"}>
                                        <FormControl fullWidth>
                                            <InputLabel htmlFor="amount">Price per Hour</InputLabel>
                                            <Input
                                                id="amount"
                                                defaultValue={this.state.lot.pricePerHour}
                                                onChange={e => {
                                                    let newLot = this.state.lot;
                                                    newLot.pricePerHour = e.target.value;
                                                    this.setState({
                                                        lot: newLot,
                                                    });
                                                }}
                                                type={"number"}
                                                startAdornment={<InputAdornment position="start">$</InputAdornment>}
                                            />
                                        </FormControl>
                                    </div>

                                    <div>
                                        <p>Available days</p>
                                        <FormControl component="fieldset">
                                            <FormGroup aria-label="position" row>
                                                {week}
                                            </FormGroup>
                                        </FormControl>
                                    </div>

                                    <div>
                                        <p>Features</p>

                                        <FormControl component="fieldset">
                                            <FormGroup aria-label="position" column>
                                                <FormControlLabel control={
                                                    <Checkbox
                                                        color="primary"
                                                        onChange={() => {
                                                            let newLot = this.state.lot;
                                                            let newFeatures = {...newLot.features};
                                                            newFeatures.keys = !newFeatures.keys;
                                                            newLot.features = newFeatures;
                                                            this.setState({
                                                                lot: newLot,
                                                            });
                                                        }}
                                                        defaultChecked={this.state.lot.features.keys}/>
                                                }
                                                                  label={"driver keeps keys"}
                                                                  labelPlacement="right"
                                                />
                                            </FormGroup>

                                            <FormGroup aria-label="position" column>
                                                <FormControlLabel control={
                                                    <Checkbox
                                                        color="primary"
                                                        onChange={() => {
                                                            let newLot = this.state.lot;
                                                            let newFeatures = {...newLot.features};
                                                            newFeatures.roof = !newFeatures.roof;
                                                            newLot.features = newFeatures;
                                                            this.setState({
                                                                lot: newLot,
                                                            });
                                                        }}
                                                        defaultChecked={this.state.lot.features.roof}/>
                                                }
                                                                  label={"roof on top of the lot"}
                                                                  labelPlacement="right"
                                                />
                                            </FormGroup>

                                            <FormGroup aria-label="position" column>
                                                <FormControlLabel control={
                                                    <Checkbox
                                                        color="primary"
                                                        onChange={() => {
                                                            let newLot = this.state.lot;
                                                            let newFeatures = {...newLot.features};
                                                            newFeatures.insurance = !newFeatures.insurance;
                                                            newLot.features = newFeatures;
                                                            this.setState({
                                                                lot: newLot,
                                                            });
                                                        }}
                                                        defaultChecked={this.state.lot.features.insurance}/>
                                                }
                                                                  label={"lot covered by insurance"}
                                                                  labelPlacement="right"
                                                />
                                            </FormGroup>

                                            <FormGroup aria-label="position" column>
                                                <FormControlLabel control={
                                                    <Checkbox
                                                        color="primary"
                                                        onChange={() => {
                                                            let newLot = this.state.lot;
                                                            let newFeatures = {...newLot.features};
                                                            newFeatures.guards = !newFeatures.guards;
                                                            newLot.features = newFeatures;
                                                            this.setState({
                                                                lot: newLot,
                                                            });
                                                        }}
                                                        defaultChecked={this.state.lot.features.guards}/>
                                                }
                                                                  label={"lot guarded by people"}
                                                                  labelPlacement="right"
                                                />
                                            </FormGroup>

                                            <FormGroup aria-label="position" column>
                                                <FormControlLabel control={
                                                    <Checkbox
                                                        color="primary"
                                                        onChange={() => {
                                                            let newLot = this.state.lot;
                                                            let newFeatures = {...newLot.features};
                                                            newFeatures.camera = !newFeatures.camera;
                                                            newLot.features = newFeatures;
                                                            this.setState({
                                                                lot: newLot,
                                                            });
                                                        }}
                                                        defaultChecked={this.state.lot.features.camera}/>
                                                }
                                                                  label={"security cameras"}
                                                                  labelPlacement="right"
                                                />
                                            </FormGroup>
                                        </FormControl>
                                    </div>

                                    <div className={"text-input"}>
                                        <p>Description</p>
                                        <TextField
                                            multiline
                                            fullWidth
                                            variant={"outlined"}
                                            rows="5"
                                            rowsMax="5"
                                            defaultValue={this.state.lot.description}
                                            onChange={e => {
                                                let newLot = this.state.lot;
                                                newLot.description = e.target.value;
                                                this.setState({
                                                    lot: newLot,
                                                });
                                            }}
                                            placeholder="My lot is awesome!"
                                        />
                                    </div>
                                    <hr/>

                                    <div className={"action-buttons"}>
                                        <Button
                                            size="large"
                                            color={"primary"}
                                            onClick={this.handleEditClose}
                                        >
                                            Cancel
                                        </Button>

                                        <Button type="submit" size="large"
                                                color={"primary"}
                                                variant="contained"
                                        >
                                            Save '{garage.name}'
                                        </Button>
                                    </div>

                                </div>


                                <div className={"map-block"}>
                                    <MiniMap
                                        lat={this.state.lat}
                                        lng={this.state.lng}
                                    />
                                </div>
                            </div>
                        </form>
                    </div>
                </Modal>

                <Modal open={this.state.delete}
                       onClose={this.handleDeleteClose}>
                    <Card className="delete-dialog">
                        <h2>Delete lot</h2>
                        <p>Are you sure you want to delete lot '{garage.name}'?</p>
                        <CardActions className={"options"}>
                            <Button color={"primary"} onClick={this.handleDeleteClose}>Cancel</Button>
                            <Button color={"secondary"} onClick={() => this.props.handleDeleteLot(garage)}>Delete
                                '{garage.name}'</Button>
                        </CardActions>
                    </Card>
                </Modal>

            </div>
        );
    }
}

export default EditLotItem;