const express = require('express');
var cors = require('cors');
const app = express();
app.use(cors());
var request = require("request");
var chai = require('chai'), chaiHttp = require('chai-http');

chai.use(chaiHttp);

const assert = require('assert');
//const expect = require('expect');
const expect = require('chai').expect;
//const should = require('chai').should();

function makeid(length) {
    var result           = '';
    var characters       = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    var charactersLength = characters.length;
    for ( var i = 0; i < length; i++ ) {
       result += characters.charAt(Math.floor(Math.random() * charactersLength));
    }
    return result;
 }
 
 

describe('Alfred Users Database Test', function () {
    //it del login y sign up
    it('Should login a user',function(done){
        chai.request('http://localhost:3001/user')
            .post('/login')
            .send({
            password: '123456', 
            email: 'gaslif@gmail.com'
            })
            .end(function (err, res) {
                expect(res.statusCode).to.equal(200);
                assert.equal(res.body.success,true);
                assert.equal(res.body.uid,'XjCGDc5nxKfKCKLGNRsZU9FXljk2');
                assert.equal(res.body.name,'Gaston');
                done();
            });
    })
    it('Should create a user',function(done){
        chai.request('http://localhost:3001/user')
            .post('/signUp')
            .send({
            password: '123456', 
            email: makeid(4) + '@gmail.com',
            name: 'Pepe',
            surname: 'Pepe'
            })
            .end(function (err, res) {
                expect(res.statusCode).to.equal(200);
                assert.equal(res.body.success,true);
                done();
            });
    })
});