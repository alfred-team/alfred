import React from 'react';
import TopNav from './TopNav';
import Stage from './Stage';
import './css/App.css';
import database from './middleware/database.js';


class App extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            zones: [],
            activeZone: {
                code: "NULL",
                lat: -34.573277,
                lng: -58.420639,
                name: "Initial zone",
            },
            activeLot: null,
            garages: [],
            activeMode: "search",
            auth: false,
            userInfo: {
                name: "",
                surname: "",
                email: "",
                uid: "",
            },
            book: false,
        };
    }

    async componentDidMount() {
        await database.getGarages(this);
        await database.getZones(this);
        this.setState({
            activeZone: this.state.zones[4],
            activeLot: null,
        })
    }

    changeActiveZone(zone) {
        this.setState({
            activeZone: zone,
            activeLot: null,
        });
    }

    changeActiveLot(lot) {
        //console.log(lot);
        this.setState({
            activeLot: lot,
        });
    }

    changeActiveMode(mode) {
        this.setState({
            activeMode: mode,
            activeLot: null,
        });
    }

    async handleSignUp(email, password, name, surname) {
        const user = await database.signUp(email, password, name, surname);
        if (user.data.success === true) {
            //Store the uid thats placed in user.data.uid
            //this.setState({ uid: user.data.uid })
            this.setState({
                activeMode: "search",
                auth: true,
                userInfo: {
                    name: name,
                    surname: surname,
                    email: email,
                    uid: user.data.uid,
                }
            });
        } else {
            //Throw visual error
            //TODO: anunciar
        }
    }

    async handleLogIn(email, password) {
        const user = await database.login(email, password);
        if (user.data.success === true) {
            //Store the uid thats placed in user.data.uid
            //this.setState({ uid: user.data.uid })
            this.setState({
                activeMode: "search",
                auth: true,
                userInfo: {
                    name: user.data.name,
                    surname: user.data.surname,
                    email: email,
                    uid: user.data.uid,
                }
            });
        } else {
            //Throw visual error
            //TODO: anunciar
        }
    }

    async handleBook(garage) {
        if (typeof garage !== "undefined") {
            if (this.state.auth === true) {
                console.log("GARAGE IS " + garage);
                this.setState({
                    activeMode: "book",
                    book: true,
                    garage: garage,
                });
            } else {
                console.log("Error. User not authenticated");
            }
        }
    }

    async handleBooking(date, init_time, end_time) {
        console.log("Date and time: ", date, init_time, end_time);
        console.log("Garage is: ", this.state.garage);
        console.log("a verga: ", this.state.userInfo.uid, date.substring(0, 4), date.substring(5, 7), date.substring(8, 10));
        var sss = new Date(date + 'T00:00:00');
        //sss.setMonth(Number(date.substring(0,3)));
        //sss.setMonth(Number(date.substring(5,6)));
        //sss.setFullYear(Number(date.substring(8,9)));
        console.log(sss);
        this.setState({
            activeMode: "search",
        });
        const res = await database.bookGarage(this.state.garage.gid, this.state.userInfo.uid, sss, init_time, end_time);
        if (res.data.success) {
            console.log('Lo pude hacer');
        } else {
            console.log('No pude');
        }
    }

    async handleAddLot(lot) {
        console.log(lot);
        const res = await database.addGarage(lot.name, lot.description, lot.pricePerHour, lot.openTime, lot.closingTime, lot.availableDays, lot.address, this.state.userInfo.uid, lot.features, lot.telephone);
        console.log("aver", res);
        if (res.data.success === true) {
            await database.getGarages(this);
        } else {
            //Avisar
        }
    }

    async handleDeleteLot(garage) {
        //Delete a lot
        const res = await database.deleteGarage(garage.gid, this.state.userInfo.uid);
        if (res.data.success === true) {
            var newGarageList = this.state.garages;
            var index = newGarageList.indexOf(garage);
            if (index !== -1) {
                newGarageList.splice(index, 1);
                this.setState({
                    garages: newGarageList,
                })
            }
        } else {
            //Avisar
        }
    }

    async handleEditLot(oldGarage, newGarage) {
        //Edit Lot
        const res = await database.editGarage(oldGarage.gid, newGarage, this.state.userInfo.uid);
        if (res.data.success === true) {
            var newGarageList = this.state.garages;
            var index = newGarageList.indexOf(oldGarage);
            if (index !== -1) {
                newGarageList.splice(index, 1);
                this.setState({
                    garages: newGarageList.concat(newGarage),
                })
            }
        }
    }

    render() {
        var activeZone = this.state.activeZone;
        var activeLot = this.state.activeLot;
        var activeMode = this.state.activeMode;
        var auth = this.state.auth;
        var userInfo = this.state.userInfo;
        return (
            <div>
                <div className={"container"}>
                    <TopNav
                        userInfo={userInfo}
                        loggedIn={auth}
                        activeMode={activeMode}
                        changeActiveMode={(mode) => this.changeActiveMode(mode)}
                    />
                </div>
                <Stage
                    handleAddLot={(lot) => this.handleAddLot(lot)}
                    handleLogIn={(email, password) => this.handleLogIn(email, password)}
                    handleSignUp={(email, password, name, surname) => this.handleSignUp(email, password, name, surname)}
                    handleDeleteLot={(garage) => this.handleDeleteLot(garage)}
                    handleEditLot={(oldGarage, newGarage) => this.handleEditLot(oldGarage, newGarage)}
                    activeMode={activeMode}
                    zones={this.state.zones}
                    activeZone={activeZone}
                    activeLot={activeLot}
                    handleBook={(garage) => this.handleBook(garage)}
                    //handleBook={this.handleBook}
                    handleBooking={(date, init_time, end_time) => this.handleBooking(date, init_time, end_time)}
                    changeActiveMode={(mode) => this.changeActiveMode(mode)}
                    changeActiveZone={(zone) => this.changeActiveZone(zone)}
                    changeActiveLot={(lot) => this.changeActiveLot(lot)}
                    userGarages={this.state.garages.filter((garage) => {
                        if (garage === null)
                            return null;
                        return garage.owner === this.state.userInfo.uid;
                    })}
                    garages={this.state.garages.filter(function (garage) {
                        if (garage === null)
                            return null;
                        return garage.address.zone === activeZone.code;
                    })}
                />
            </div>
        );
    }
}

export default App;
