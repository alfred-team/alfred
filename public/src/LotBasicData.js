import React from 'react';
import './css/LotBasicData.css';

import CardContent from "@material-ui/core/CardContent";
import MapIcon from '@material-ui/icons/Map';
import ScheduleIcon from '@material-ui/icons/Schedule';
import PhoneIcon from '@material-ui/icons/Phone';

class LotBasicData extends React.Component {
    render() {
        let garage = this.props.garage;

        return (
            <div>
                <img className={"CarImage"} src={"https://tinyurl.com/placeholder-lot"} alt="Placeholder Lot"/>
                <CardContent>
                    <div className={"lot-flex"}>
                        <div className={"lot-grid"}>
                            <div className={"LotData"}>
                                <h4 className={"inline"}>{garage.name}</h4>
                                <br/>
                                <br/>
                            </div>

                            <div className={"LotPrice"}>
                                <h2 className={"inline"}>$</h2>
                                <h2 className={"inline"}>{garage.pricePerHour}</h2>
                            </div>
                        </div>

                        <div>
                            <MapIcon className={"icon"} color="inherit"/>
                            <p className={"inline"}>{garage.address.description}</p>
                            <p className={"inline"}>, </p>
                            <p className={"inline"}>{garage.address.zone}</p>
                            <br/>
                        </div>

                        <div>
                            <ScheduleIcon className={"icon"} color="inherit"/>
                            <p className={"inline"}>{garage.openTime}</p>
                            <p className={"inline"}> - </p>
                            <p className={"inline"}>{garage.closingTime}</p>
                        </div>

                        <div>
                            <PhoneIcon className={"icon"} color="inherit"/>
                            <p className={"inline"}>{garage.telephone}</p>
                        </div>


                    </div>

                </CardContent>
            </div>
        );

    }
}

export default LotBasicData;