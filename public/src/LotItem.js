import React from 'react';
import './css/LotBasicData.css';

import LotBasicData from "./LotBasicData";
//from material ui
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import Button from '@material-ui/core/Button';
import Box from "@material-ui/core/Box";
import ExpansionPanel from '@material-ui/core/ExpansionPanel';
import ExpansionPanelSummary from '@material-ui/core/ExpansionPanelSummary';
import ExpansionPanelDetails from '@material-ui/core/ExpansionPanelDetails';

import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import DateRangeIcon from '@material-ui/icons/DateRange';
import CheckIcon from '@material-ui/icons/Check';
import ClearIcon from '@material-ui/icons/Clear';


class LotItem extends React.Component {

    render() {
        let garage = this.props.garage;
        return (
            <Card onClick={() => this.props.changeActiveLot(garage)} className={"LotCard"}>

                <LotBasicData garage={garage}/>

                <CardActions className={"details"}>
                    <ExpansionPanel className={"details"}>
                        <ExpansionPanelSummary
                            expandIcon={<ExpandMoreIcon/>}
                            aria-controls="Lot-content"
                            id="Lot-header"
                        >
                            <h4>Details</h4>
                        </ExpansionPanelSummary>
                        <ExpansionPanelDetails>
                            <Box>

                                <div>
                                    <DateRangeIcon className={"icon"} color="inherit"/>
                                    <h5 className={garage.availableDays[0] ? "inline boxed green" : "inline boxed red"}>S</h5>
                                    <h5 className={garage.availableDays[1] ? "inline boxed green" : "inline boxed red"}>M</h5>
                                    <h5 className={garage.availableDays[2] ? "inline boxed green" : "inline boxed red"}>T</h5>
                                    <h5 className={garage.availableDays[3] ? "inline boxed green" : "inline boxed red"}>W</h5>
                                    <h5 className={garage.availableDays[4] ? "inline boxed green" : "inline boxed red"}>T</h5>
                                    <h5 className={garage.availableDays[5] ? "inline boxed green" : "inline boxed red"}>F</h5>
                                    <h5 className={garage.availableDays[6] ? "inline boxed green" : "inline boxed red"}>S</h5>

                                </div>
                                <br/>

                                <div>
                                    <h5>Features</h5>
                                    <div className={garage.features.keys ? "green-text" : "red-text"}>
                                        <CheckIcon className={garage.features.keys ? "icon" : "icon hide"}
                                                   color="inherit"/>
                                        <ClearIcon className={garage.features.keys ? "icon hide" : "icon"}
                                                   color="inherit"/>
                                        <p className={"inline"}>driver keeps keys</p>
                                    </div>
                                    <div className={garage.features.roof ? "green-text" : "red-text"}>
                                        <CheckIcon className={garage.features.roof ? "icon" : "icon hide"}
                                                   color="inherit"/>
                                        <ClearIcon className={garage.features.roof ? "icon hide" : "icon"}
                                                   color="inherit"/>
                                        <p className={"inline"}>roof on top of the lot</p>
                                    </div>
                                    <div className={garage.features.insurance ? "green-text" : "red-text"}>
                                        <CheckIcon className={garage.features.insurance ? "icon" : "icon hide"}
                                                   color="inherit"/>
                                        <ClearIcon className={garage.features.insurance ? "icon hide" : "icon"}
                                                   color="inherit"/>
                                        <p className={"inline"}>lot covered by insurance</p>
                                    </div>
                                    <div className={garage.features.guards ? "green-text" : "red-text"}>
                                        <CheckIcon className={garage.features.guards ? "icon" : "icon hide"}
                                                   color="inherit"/>
                                        <ClearIcon className={garage.features.guards ? "icon hide" : "icon"}
                                                   color="inherit"/>
                                        <p className={"inline"}>lot guarded by people</p>
                                    </div>
                                    <div className={garage.features.camera ? "green-text" : "red-text"}>
                                        <CheckIcon className={garage.features.camera ? "icon" : "icon hide"}
                                                   color="inherit"/>
                                        <ClearIcon className={garage.features.camera ? "icon hide" : "icon"}
                                                   color="inherit"/>
                                        <p className={"inline"}>security cameras</p>
                                    </div>
                                </div>
                                <br/>

                                <h5>About my Lot!</h5>
                                <p>{garage.description}</p>
                            </Box>
                        </ExpansionPanelDetails>
                    </ExpansionPanel>

                </CardActions>

                <CardActions className={"options"}>
                    <Button color={"primary"} onClick={() => this.props.handleBook(garage)}>Book this lot</Button>
                </CardActions>
            </Card>
        );
    }
}

export default LotItem;
