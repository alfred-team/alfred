import React from 'react';
import './css/SearchBar.css';
//material ui components
import Paper from '@material-ui/core/Paper';
import InputBase from '@material-ui/core/InputBase';
import Divider from '@material-ui/core/Divider';
import IconButton from '@material-ui/core/IconButton';
import SearchIcon from '@material-ui/icons/Search';
import ExpansionPanelSummary from "@material-ui/core/ExpansionPanelSummary";
import ExpansionPanelDetails from "@material-ui/core/ExpansionPanelDetails";
import Box from "@material-ui/core/Box";
import ExpansionPanel from "@material-ui/core/ExpansionPanel";
import FormControl from "@material-ui/core/FormControl";
import InputLabel from "@material-ui/core/InputLabel";
import Select from "@material-ui/core/Select";
import MenuItem from '@material-ui/core/MenuItem';


class SearchBar extends React.Component {
    displayOptions = () => {
        return this.props.zones.map((zone, index) => {
            return <MenuItem key={index} value={zone}>{zone.name}</MenuItem>
        })
    };

    render() {
        return (
            <div>
                <Paper className={"searcher"}>
                    <InputBase
                        className={"input"}
                        placeholder="Search lot"
                        inputProps={{'aria-label': 'search lot'}}
                        onChange={(event: object) => this.props.onChange(event.target.value)}
                    />
                    <IconButton className={"iconButton"} aria-label="search">
                        <SearchIcon/>
                    </IconButton>
                    <Divider className={"divider"} orientation="vertical"/>
                </Paper>


                <ExpansionPanel className={"filterPanel"}>
                    <ExpansionPanelSummary
                        aria-controls="myFilters"
                        id="Lot-Filter"
                    >
                        <h4>My filters</h4>
                    </ExpansionPanelSummary>
                    <ExpansionPanelDetails>
                        <Box className={"maxWidth"}>
                            <FormControl className={"zoneFilter filterBox"}>
                                <InputLabel>Zone</InputLabel>
                                <Select
                                    value={this.props.activeZone}
                                    onChange={(event: object) => this.props.changeActiveZone(event.target.value)}
                                    inputProps={{
                                        name: 'zone',
                                        id: 'zone-native-simple',
                                    }}
                                    renderValue={(value: any) => {
                                        return this.props.activeZone.name;
                                    }}
                                >
                                    {this.displayOptions()}
                                </Select>
                            </FormControl>
                        </Box>
                    </ExpansionPanelDetails>
                </ExpansionPanel>
            </div>
        );
    }
}

export default SearchBar;