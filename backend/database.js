var firebase = require("firebase/app");

// Add the Firebase products that you want to use
require("firebase/auth");
require("firebase/database");

exports.getGarages = async function(){
    await firebase.database().ref('garage').once('value').
    then(( snapshot )=>{
        return snapshot.val();
    }).catch((error) => {
      console.log(error);
    });
}
function makeid(length) {
   var result           = '';
   var characters       = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
   var charactersLength = characters.length;
   for ( var i = 0; i < length; i++ ) {
      result += characters.charAt(Math.floor(Math.random() * charactersLength));
   }
   return result;
}
exports.newGarage = function( name, description, pricePerHour, openTime, closingTime, availableDays, address, uid, res, features, telephone ){
  garageRef = firebase.database().ref('garage');
  const gid = makeid(10);
  garageRef.update({
    [gid]: {
      address: address,
      name: name,
      description: description,
      pricePerHour: pricePerHour,
      openTime: openTime,
      closingTime: closingTime,
      availableDays: availableDays,
      img: "garages/cars-on-a-lot-near-trees.jpg",
      owner: uid,
      "features": features,
      "telephone": telephone
    }
  }).then((response) => {
    res.json({ success: true, data: response });
  }).catch((err) => {
    json.res({ success: false, error: err });
    console.log(err);
  });
}



exports.newVehicle = function( email, password, vehicle ){
  firebase
  .auth()
  .signInWithEmailAndPassword(email, password)
  .then(res => {
    firebase.database().ref('users/' + res.user.uid + '/vehicles' ).push( vehicle )
    .catch(e => {
      console.log(e);
    });
  })
  .catch(e => {
    console.log(e);
  });

}

exports.editProfile = function( email, password, newSettings ){
  firebase
  .auth()
  .signInWithEmailAndPassword( email, password )
  .then( res => {
    firebase.database().ref('users/' + res.user.uid ).update( newSettings )
    .catch(e => {
      console.log(e);
    });
  })
  .catch( e => {
    console.log(e);
  })
}

exports.editGarage = function( gid, newSettings,uid,res ){
  firebase.database().ref('garage/' + gid).once('value')
    .then((snapshot) => {
      if(snapshot.val().owner === uid){
        firebase.database().ref('garage/' + gid ).update( newSettings )
        res.json({ success:true });
      }else{
        res.json({success: false, error: "you dont have permissions"});
      }
    })
    .catch((error) => {
      res.json({ error, success:false });
    });
}

exports.getOwnerGarages = async function( uid ){
    return firebase.database().ref('garage').once('value').
    then(( snapshot )=>{
        return snapshot.val().filter(item => {
          return item.owner === uid;
        });
    }).catch((e) => {
      console.log(e);
    });
}

exports.deleteGarage = async function( gid, uid, res ){
  firebase.database().ref('garage/' + gid).once('value').
  then(( snapshot )=>{
    console.log(snapshot.val());
    if(snapshot.val().owner === uid){
      garageRef = firebase.database().ref('garage/' + gid ).remove();
      res.json({ success:true });
    } else {
      res.json({ success:false, error: 'you dont have permissions' });
    }
  }).catch((error) => {
    res.json({ success:false, err: error });
  });
}

function hourRangeOverlaps(a_start, a_end, b_start, b_end) {
  if (a_start <= b_start && b_start <= a_end) return true; // b starts in a
  if (a_start <= b_end   && b_end   <= a_end) return true; // b ends in a
  if (b_start <  a_start && a_end   <  b_end) return true; // a in b
  return false;
}

function checkIfDateExists(scheduleArray, date,initHour, finalHour){
  var month = Number(date.getMonth())+1;
  var stringDate = date.getFullYear() + '/' + month + '/' + date.getDate();
  for(var i in scheduleArray){
    if(scheduleArray[i].date === stringDate){
      console.log(scheduleArray[i].date, stringDate);
      if(hourRangeOverlaps(scheduleArray[i].initHour,scheduleArray[i].finalHour,initHour,finalHour)){
        return true;
      }
    }
  }
  return false;
}

exports.bookGarage = async function(gid, uid, date, initHour, finalHour,res){
  var dates = new Date(date);
  var month = Number(dates.getMonth())+1;
  firebase.database().ref('garage/' + gid).once('value')
    .then((snapshot) => {
      var schedule = snapshot.val().schedule;
      if(schedule == null){
        schedule = [];
      }
        if(checkIfDateExists(schedule,dates, initHour, finalHour)){
          res.json({ success:false, err: 'The date you chose is already booked' });
        }else{
          var obj = {
            "date"     : dates.getFullYear() + '/' + month + '/' + dates.getDate(),
            "initHour" : initHour,
            "finalHour": finalHour,
            "uid"      : uid   
          };
          schedule.push(obj);
          firebase.database().ref('garage/' + gid).update({"schedule": schedule});
          res.json({ success: true });
        }
    })
    .catch((error) => {
      res.json({ success:false, err: error });
      console.log(error);
    });
}