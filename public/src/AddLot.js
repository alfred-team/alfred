import React from 'react';
import './css/AddLot.css';
import TextField from '@material-ui/core/TextField';
import Button from "@material-ui/core/Button";
import FormControl from "@material-ui/core/FormControl";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import FormGroup from "@material-ui/core/FormGroup";
import Select from "@material-ui/core/Select";
import Input from "@material-ui/core/Input";
import InputLabel from "@material-ui/core/InputLabel";
import InputAdornment from "@material-ui/core/InputAdornment";
import {Checkbox} from "@material-ui/core";
import Modal from '@material-ui/core/Modal';
import MiniMap from './MiniMap';
import Geocode from "react-geocode";

import AddIcon from '@material-ui/icons/Add';


class AddLot extends React.Component {


    constructor(props) {
        super(props);
        this.state = {
            open: false,
            name: "",
            address: {
                description: "",
                lat: 0,
                lng: 0,
                zone: "",
            },
            openTime: "06:00",
            closingTime: "22:00",
            pricePerHour: 100,
            telephone: "12 1234-5678",
            availableDays: [false, false, false, false, false, false, false],
            features: {
                keys: false,
                roof: false,
                insurance: false,
                guards: false,
                camera: false
            },
            description: "my lot is awesome!",
            activeZone: {
                name: "My neighborhood...",
                lat: 0,
                lng: 0,
                code: "NULL",
            },
        };
    }

    handleOpen = () => {
        this.setState({open: true});
    };

    handleClose = () => {
        this.setState({open: false});
    };

    handleGeocode = (address) => {
        Geocode.fromAddress(address).then(
            response => {
                const {lat, lng} = response.results[0].geometry.location;
                this.setState({
                    address: {
                        description: this.state.address.description,
                        lat: lat,
                        lng: lng,
                        zone: this.state.address.zone,
                    }
                })
            },
            error => {
                console.error(error);
            }
        );
    };

    displayOptions = () => {
        return this.props.zones.map((zone, index) => {
            return <option key={index} value={zone}>{zone.name}</option>
        })
    };

    render() {
        let days = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday"];
        let week = [];

        for (let i = 0; i < 7; i++) {
            week.push(
                <FormControlLabel key={i}
                                  control={
                                      <Checkbox
                                          color="primary"
                                          onChange={() => {
                                              let newAvailableDays = this.state.availableDays.slice();
                                              newAvailableDays[i] = !newAvailableDays[i];
                                              this.setState({
                                                  availableDays: newAvailableDays,
                                              });
                                          }}
                                          defaultChecked={this.state.availableDays[i]}/>
                                  }
                                  label={days[i]}
                                  labelPlacement="top"
                />
            );
        }

        return (
            <div>
                <Button variant="contained" size={"large"} color="primary" className={"new-lot-button"}
                        onClick={this.handleOpen}>
                    <AddIcon color="inherit"/>
                    <p color="inherit">Add new lot</p>
                </Button>

                <Modal open={this.state.open}
                       onClose={this.handleClose}
                       className={"dialog-container"}>
                    <div className="dialog-main">

                        <form
                            onSubmit={e => {
                                e.preventDefault();
                                var lot = {
                                    name: this.state.name,
                                    address: this.state.address,
                                    openTime: this.state.openTime,
                                    closingTime: this.state.closingTime,
                                    description: this.state.description,
                                    availableDays: this.state.availableDays,
                                    pricePerHour: this.state.pricePerHour,
                                    telephone: this.state.telephone,
                                    features: this.state.features,
                                };
                                this.props.handleAddLot(lot);
                                this.handleClose();
                                return false;
                            }}>

                            <div className={"hor-flex-container"}>
                                <div className={"lot-flex-container half-size"}>

                                    <h1>Add new lot</h1>

                                    <div className={"text-input"}>
                                        <p>Lot name</p>
                                        <TextField
                                            id="name"
                                            placeholder={"my lot's name is..."}
                                            fullWidth
                                            onChange={e => {
                                                this.setState({
                                                    name: e.target.value,
                                                });
                                            }}
                                            type={"text"}
                                        />
                                    </div>

                                    <div className={"text-input"}>
                                        <p>Lot address</p>
                                        <TextField
                                            id="address"
                                            placeholder={"my lot is in..."}
                                            fullWidth
                                            onChange={e => {
                                                this.setState({
                                                    address: {
                                                        description: e.target.value,
                                                        lat: this.state.address.lat,
                                                        lng: this.state.address.lng,
                                                        zone: this.state.address.zone,
                                                    }
                                                });
                                                this.handleGeocode(e.target.value);
                                            }}
                                            type={"text"}
                                        />
                                    </div>

                                    <FormControl className={"text-input"}>
                                        <p>Zone</p>
                                        <Select
                                            value={this.state.activeZone}
                                            onChange={(event: object) => {
                                                this.setState({
                                                    address: {
                                                        description: this.state.address.description,
                                                        lat: this.state.address.lat,
                                                        lng: this.state.address.lng,
                                                        zone: event.target.value.code,
                                                    },
                                                    activeZone: event.target.value,
                                                });
                                            }}
                                            inputProps={{
                                                name: 'zone',
                                                id: 'zone-native-simple',
                                            }}
                                            renderValue={(value: any) => {
                                                return this.state.activeZone.name;
                                            }}
                                        >
                                            {this.displayOptions()}
                                        </Select>
                                    </FormControl>


                                    <div className={"hor-flex-container"}>
                                        <div className={"text-input quarter-size"}>
                                            <p>Opening time</p>
                                            <TextField
                                                id="opening-time"
                                                name="openingTime"
                                                type="time"
                                                defaultValue={this.state.openTime}
                                                onChange={e => {
                                                    this.setState({
                                                        openTime: e.target.value,
                                                    });
                                                }}
                                                inputProps={{
                                                    step: 300, // 5 min
                                                }}
                                            />
                                        </div>

                                        <div className={"text-input quarter-size"}>
                                            <p>Closing time</p>
                                            <TextField
                                                id="opening-time"
                                                name="closingTime"
                                                type="time"
                                                defaultValue={this.state.closingTime}
                                                onChange={e => {
                                                    this.setState({
                                                        closingTime: e.target.value,
                                                    });
                                                }}
                                                inputProps={{
                                                    step: 300, // 5 min
                                                }}
                                            />
                                        </div>
                                    </div>

                                    <div className={"text-input"}>
                                        <p>Phone number</p>
                                        <TextField
                                            id="phone"
                                            name="phoneField"
                                            defaultValue={this.state.telephone}
                                            fullWidth
                                            onChange={e => {
                                                this.setState({
                                                    telephone: e.target.value,
                                                });
                                            }}
                                            type={"text"}
                                        />
                                    </div>


                                    <div className={"text-input"}>
                                        <FormControl fullWidth>
                                            <InputLabel htmlFor="amount">Price per Hour</InputLabel>
                                            <Input
                                                id="amount"
                                                defaultValue={this.state.pricePerHour}
                                                onChange={e => {
                                                    this.setState({
                                                        pricePerHour: e.target.value,
                                                    });
                                                }}
                                                type={"number"}
                                                startAdornment={<InputAdornment position="start">$</InputAdornment>}
                                            />
                                        </FormControl>
                                    </div>

                                    <div>
                                        <p>Available days</p>
                                        <FormControl component="fieldset">
                                            <FormGroup aria-label="position" row>
                                                {week}
                                            </FormGroup>
                                        </FormControl>
                                    </div>

                                    <div>
                                        <p>Features</p>

                                        <FormControl component="fieldset">
                                            <FormGroup aria-label="position" column>
                                                <FormControlLabel control={
                                                    <Checkbox
                                                        color="primary"
                                                        onChange={() => {
                                                            let newFeatures = this.state.features;
                                                            newFeatures.keys = !newFeatures.keys;
                                                            this.setState({
                                                                features: newFeatures,
                                                            });
                                                        }}
                                                        defaultChecked={this.state.features.keys}/>
                                                }
                                                                  label={"driver keeps keys"}
                                                                  labelPlacement="right"
                                                />
                                            </FormGroup>

                                            <FormGroup aria-label="position" column>
                                                <FormControlLabel control={
                                                    <Checkbox
                                                        color="primary"
                                                        onChange={() => {
                                                            let newFeatures = this.state.features;
                                                            newFeatures.roof = !newFeatures.roof;
                                                            this.setState({
                                                                features: newFeatures,
                                                            });
                                                        }}
                                                        defaultChecked={this.state.features.roof}/>
                                                }
                                                                  label={"roof on top of the lot"}
                                                                  labelPlacement="right"
                                                />
                                            </FormGroup>

                                            <FormGroup aria-label="position" column>
                                                <FormControlLabel control={
                                                    <Checkbox
                                                        color="primary"
                                                        onChange={() => {
                                                            let newFeatures = this.state.features;
                                                            newFeatures.insurance = !newFeatures.insurance;
                                                            this.setState({
                                                                features: newFeatures,
                                                            });
                                                        }}
                                                        defaultChecked={this.state.features.insurance}/>
                                                }
                                                                  label={"lot covered by insurance"}
                                                                  labelPlacement="right"
                                                />
                                            </FormGroup>

                                            <FormGroup aria-label="position" column>
                                                <FormControlLabel control={
                                                    <Checkbox
                                                        color="primary"
                                                        onChange={() => {
                                                            let newFeatures = this.state.features;
                                                            newFeatures.guards = !newFeatures.guards;
                                                            this.setState({
                                                                features: newFeatures,
                                                            });
                                                        }}
                                                        defaultChecked={this.state.features.guards}/>
                                                }
                                                                  label={"lot guarded by people"}
                                                                  labelPlacement="right"
                                                />
                                            </FormGroup>

                                            <FormGroup aria-label="position" column>
                                                <FormControlLabel control={
                                                    <Checkbox
                                                        color="primary"
                                                        onChange={() => {
                                                            let newFeatures = this.state.features;
                                                            newFeatures.camera = !newFeatures.camera;
                                                            this.setState({
                                                                features: newFeatures,
                                                            });
                                                        }}
                                                        defaultChecked={this.state.features.camera}/>
                                                }
                                                                  label={"security cameras"}
                                                                  labelPlacement="right"
                                                />
                                            </FormGroup>
                                        </FormControl>
                                    </div>


                                    <div className={"text-input"}>
                                        <p>Description</p>
                                        <TextField
                                            multiline
                                            fullWidth
                                            variant={"outlined"}
                                            rows="5"
                                            rowsMax="5"
                                            defaultValue={this.state.description}
                                            onChange={e => {
                                                this.setState({
                                                    description: e.target.value,
                                                });
                                            }}
                                            placeholder="My lot is awesome!"
                                        />
                                    </div>
                                    <hr/>

                                    <div className={"action-buttons"}>
                                        <Button
                                            onClick={this.handleClose}
                                            size="large"
                                            color={"secondary"}
                                        >
                                            Cancel
                                        </Button>
                                        <Button type="submit" size="large"
                                                color={"primary"}
                                                variant="contained">
                                            Add lot
                                        </Button>
                                    </div>
                                </div>

                                <div className={"map-block"}>
                                    <MiniMap
                                        lat={this.state.address.lat}
                                        lng={this.state.address.lng}
                                    />
                                </div>
                            </div>


                        </form>
                    </div>
                </Modal>


            </div>
        );
    }
}


export default AddLot;