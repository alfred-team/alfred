const express = require('express');
var cors = require('cors');
const app = express();
app.use(cors());
var request = require("request");

var chai = require('chai'), chaiHttp = require('chai-http');

chai.use(chaiHttp);

const assert = require('assert');
//const expect = require('expect');
const expect = require('chai').expect;
//const should = require('chai').should();

function makeid(length) {
    var result           = '';
    var characters       = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    var charactersLength = characters.length;
    for ( var i = 0; i < length; i++ ) {
       result += characters.charAt(Math.floor(Math.random() * charactersLength));
    }
    return result;
 }

describe('Alfred Garage Database Test', function () {
    //no olvidar booking de un garage
    it('should return Buenos Aires City garages', function (done) {
        var url = "http://localhost:3001/garage/getGarages";
        request(url, function(error,response,body){
            var obj = JSON.parse(body);
            expect(error).to.be.null;
            expect(obj.success).to.be.true;
            if(obj.data != undefined){
                for(var i in obj.data){
                    expect(obj.data[i].openTime).to.be.a('string');
                    expect(obj.data[i].closingTime).to.be.a('string');
                    expect(obj.data[i].img).to.be.a('string');
                    expect(obj.data[i].name).to.be.a('string');
                    expect(obj.data[i].owner).to.be.a('string');
                    expect(obj.data[i].availableDays).to.be.an('array');
                    expect(obj.data[i].address).to.be.an('object');
                }
            }
            done();
        });
    });
    it('should edit a random garage with randomize description', function (done) {
        chai.request('http://localhost:3001/garage')
            .post('/editGarage')
            .send({
            gid: 'AVXAPzpK9s', 
            newSettings: {
                "address" : {
                  "description" : "Tucuman 654, CABA",
                  "lat" : -34.6011479,
                  "lng" : -58.3759853,
                  "zone" : "RET"
                },
                "availableDays" : [ false, true, true, true, true, true, false ],
                "closingTime" : "22:00",
                "description" : "my lot is awesome!! with some modification " + makeid(4) + "\n",
                "features" : {
                  "camera" : false,
                  "guards" : true,
                  "insurance" : false,
                  "keys" : false,
                  "roof" : true
                },
                "gid" : "AVXAPzpK9s",
                "img" : "garages/cars-on-a-lot-near-trees.jpg",
                "name" : "Alfred",
                "openTime" : "06:00",
                "owner" : "vFbCKADKU5d1rkXRvumC7iWonIU2",
                "pricePerHour" : "150",
                "telephone" : "11 8930-4932"
              },              
            uid: 'vFbCKADKU5d1rkXRvumC7iWonIU2'
            })
            .end(function (err, res) {
                expect(res.statusCode).to.equal(200);
                assert.equal(res.body.success,true);
                done();
            });
    });
    
});